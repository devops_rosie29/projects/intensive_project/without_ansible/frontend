FROM node:14-alpine AS build

WORKDIR /app_front

COPY ./package.json /app_front/package.json
RUN npm install
COPY . /app_front/
RUN npm run build

FROM nginx:1.16.0-alpine

COPY --from=build /app_front/build /usr/share/nginx/html

EXPOSE 3000
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
